#include "CpuInfo.h"

void CpuInfo::initCpuInfo()
{
    std::string prefix_ = "/sys/devices/system/cpu/cpu";
    std::string sufix1_ = "/cache/index";
    std::string sufix2_ = "/shared_cpu_list";
    std::vector<int> done (this->numCpus * this->numSimblings);
    for (int i = 0; i < this->numCpus; i++) {
        for (int j = 0; j < this->numSimblings; j++) {
            if (done[i * numSimblings + j])
                continue;
            std::string prefix = prefix_;
            std::string sufix1 = sufix1_;
            std::string sufix2 = sufix2_;
            std::string index = std::to_string(j);
            std::string cpu = std::to_string(i);
            std::string path1 = prefix.append(cpu);
            std::string path2 = path1.append(sufix1);
            std::string path3 = path2.append(index);
            std::string total_path = path3.append(sufix2);
            std::ifstream in;
            try {
                in.open(total_path);
            } catch (...) {
                std::cerr << "Error opening file: " << total_path << std::endl;
                (*simblings)[i * numSimblings + j] = 0;
                continue;
            }
            std::string all;
            in >> all;
            in.close();

            std::vector<std::string> tokens = getLimits(all,',');
            int numTokens = tokens.size();
            for(int i = 0; i < numTokens; i++) {
                std::vector<std::string> nums = getLimits(tokens[i],'-');
                int nLimits = nums.size();
                if (nLimits == 1) {
                    int index = std::stoi(nums[0]) * numSimblings + j;
                    done[index] = 1;
                    (*simblings)[index] = (*groupCounters)[j];
                } else if (nLimits == 2) {
                    int base = std::stoi(nums[0]), limit = std::stoi(nums[1]);
                    for (int k = base; k <= limit; k++) {
                        int index = k * numSimblings + j;
                        done[index] = 1;
                        (*simblings)[index] = (*groupCounters)[j];
                    }
                }
            }
            (*groupCounters)[j]++;
        }
    }
}

CpuInfo::CpuInfo()
{
    this->numCpus = get_nprocs();
    this->numSimblings = this->getNumCaches();
    this->simblings = new std::vector<int> (this->numCpus * this->numSimblings);
    this->groupCounters = new std::vector<int> (this->numSimblings);
    for(int i = 0; i < this->numSimblings; i++) {
        (*groupCounters)[i] = 0;
        for(int j = 0; j < this->numCpus; j++) {
            (*simblings)[i * this->numCpus + j] = 0;
        }
    }
    initCpuInfo();
}

std::string CpuInfo::toString()
{
    std::string str = "";
    for(int i = 0; i < this->numSimblings; i ++) {
        str.append("num groups of level ").append(std::to_string(i)).append(":\t");
        str.append(std::to_string((*groupCounters)[i])).append("\n");
    }
    for (int i = 0; i < this->numCpus; i++) {
        str.append("\nCPU:\t").append(std::to_string(i)).append("\n");
        for (int j = 0; j < this->numSimblings; j++) {
            str.append("L").append(std::to_string(j + 1)).append(" group:\t");
            str.append(std::to_string(getGroup(i,j))).append("\n");
        }
    }
    return str;
}
