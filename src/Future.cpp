#include "Future.h"
#include "Task.h"
#include "DataTask.h"

void Future::get__(Task * task)
{
    const ThreadWrapper * thr = task->worker;
    Scheduler * sched = thr->sched;
    int workerId = thr->workerId;
    RandomNumberGenerator * prng = thr->generator;
    Task * next = nullptr;
    while(!this->ended) {
        next = sched->getScheduledTask(workerId, prng);
        if(next != nullptr) {
            next->call(thr);
        }

        next = nullptr;
    }
}
